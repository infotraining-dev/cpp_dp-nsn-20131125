#include "template_method.hpp"

using namespace std;

void client(AbstractClass* obj)
{
    obj->template_method();
}

int main()
{
    AbstractClass* c = new ConcreteClassB;

    client(c);

	delete c;
}
