#include "bank_account.hpp"

double NormalState::NORMAL_INTEREST_RATE = 0.05;
double OverdraftState::OVERDRAFT_INTEREST_RATE = 0.15;

std::shared_ptr<AccountState> AccountState::get_next_state()
{
    if (balance_ >= 0)
        return std::shared_ptr<AccountState>(new NormalState(balance_));
    else
        return std::shared_ptr<AccountState>(new OverdraftState(balance_));
}
