#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;

#define MOTIF

class Widget {
public:
	virtual void draw() = 0;
	virtual ~Widget() {}
};

class MotifButton : public Widget {
public:
	void draw() {
		cout << "MotifButton\n";
	}
};

class MotifMenu : public Widget {
public:
	void draw() {
		cout << "MotifMenu\n";
	}
};

class WindowsButton : public Widget {
public:
	void draw() {
		cout << "WindowsButton\n";
	}
};

class WindowsMenu : public Widget {
public:
	void draw() {
		cout << "WindowsMenu\n";
	}
};

class WidgetFactory
{
public:
    virtual Widget* create_menu() = 0;
    virtual Widget* create_button() = 0;
    virtual ~WidgetFactory() {}
};

class MotifFactory : public WidgetFactory
{
public:
    virtual Widget* create_menu()
    {
        return new MotifMenu;
    }

    virtual Widget* create_button()
    {
        return new MotifButton();
    }
};

class WindowsFactory : public WidgetFactory
{
public:
    virtual Widget* create_menu()
    {
        return new WindowsMenu;
    }

    virtual Widget* create_button()
    {
        return new WindowsButton();
    }
};

class Window
{
	std::vector<Widget*> widgets;
public:
	void display() const
	{
		std::cout << "######################\n";
		for_each(widgets.begin(), widgets.end(), std::mem_fun(&Widget::draw));
		std::cout << "######################\n\n";
	}

	void add_widget(Widget* widget)
	{
		widgets.push_back(widget);
	}

	virtual ~Window()
	{
		std::vector<Widget*>::iterator it = widgets.begin();
		for(; it != widgets.end(); ++it)
			delete *it;
	}
};

class WindowOne : public Window
{

public:
    WindowOne(WidgetFactory& f)
	{
        add_widget(f.create_button());
        add_widget(f.create_menu());

	}
};

class WindowTwo : public Window
{

public:
    WindowTwo(WidgetFactory& f)
	{

        add_widget(f.create_button());
        add_widget(f.create_menu());
        add_widget(f.create_menu());
    }
};


int main(void)
{
#ifdef WINDOWS
    WindowsFactory f;
#else
    MotifFactory f;
#endif

    WindowOne w1(f);
	w1.display();

    WindowTwo w2(f);
	w2.display();
}
