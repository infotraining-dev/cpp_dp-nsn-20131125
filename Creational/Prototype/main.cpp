#include <iostream>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
protected:
    virtual Engine* do_clone() const = 0;
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    Engine* clone() const
    {
        Engine* cloned_engine = do_clone();

        assert(typeid(*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

    ~Engine() {}
};

class Diesel : public Engine
{
public:
    virtual void start()
    {
        cout << "Diesel starts\n";
    }

    virtual void stop()
    {
        cout << "Diesel stops\n";
    }
protected:
    virtual Diesel* do_clone() const
    {
        return new Diesel(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start()
    {
        cout << "TDI starts\n";
    }

    virtual void stop()
    {
        cout << "TDI stops\n";
    }
protected:
    virtual TDI* do_clone() const
    {
        return new TDI(*this);
    }
};

class Hybrid : public Engine
{
public:
    virtual void start()
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop()
    {
        cout << "Hybrid stops\n";
    }
protected:
    virtual Hybrid* do_clone() const
    {
        return new Hybrid(*this);
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_(engine)
    {
    }

    Car(const Car& source) : engine_(source.engine_->clone())
    {
    }

    ~Car()
    {
        delete engine_;
    }

    void drive(int kms)
    {
        engine_->start();
        cout << "Driving " << kms << "kms" << endl;
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\n";

    Car cc1 = c1;

    cc1.drive(400);

    cout << "\n";

    Car c2(new Hybrid());

    c2.drive(200);

    cout << "\n";

    Car cc2 = c2;

    cc2.drive(500);
}

