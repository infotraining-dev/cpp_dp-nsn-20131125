#include "employee.hpp"
#include "hrinfo.hpp"

Employee::Employee(const std::string& name) : name_(name)
{
}

Employee::~Employee()
{
}

std::auto_ptr<HRInfo> Employee::create_hrinfo()
{
    return std::auto_ptr<HRInfo>(new StdInfo(this));
}

Salary::Salary(const std::string& name) : Employee(name)
{
}

void Salary::description() const
{
	std::cout << "Salaried Employee: " << name_ << std::endl;
}

Hourly::Hourly(const std::string& name) : Employee(name)
{
}

void Hourly::description() const
{
	std::cout << "Hourly Employee: " << name_ << std::endl;
}

Temp::Temp(const std::string& name) : Employee(name)
{
}

void Temp::description() const
{
	std::cout << "Temporary Employee: " << name_ << std::endl;
}

std::auto_ptr<HRInfo> Temp::create_hrinfo()
{
    return std::auto_ptr<HRInfo>(new TempInfo(this));
}
