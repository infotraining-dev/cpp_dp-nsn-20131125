#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <vector>
#include <algorithm>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

namespace Drawing
{

// TO DO: zaimplementowac kompozyt
// grupuj�cy kszta�ty geometryczne

class ShapeGroup : public Shape
{
public:
    typedef boost::shared_ptr<Shape> ShapePtr;

    ShapeGroup()
    {}

    ShapeGroup(const ShapeGroup& source)
    {
        for(int i = 0; i < source.shapes_.size(); ++i)
            shapes_.push_back(ShapePtr(source.shapes_[i]->clone()));
    }

    void add(ShapePtr shape)
    {
        shapes_.push_back(shape);
    }

    virtual void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::draw, _1));
    }

    virtual void move(int dx, int dy)
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::move, _1, dx, dy));
    }

    virtual ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    virtual void read(std::istream &in)
    {
        int count;
        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_id;
            in >> type_id;
            ShapePtr shape
                    = ShapePtr(ShapeFactory::instance().create(type_id));
            shape->read(in);
            add(shape);
        }
    }

    virtual void write(std::ostream &out)
    {
        std::for_each(shapes_.begin(), shapes_.end(),
                      boost::bind(&Shape::write, _1, std::ref(out)));
    }
private:
    std::vector<ShapePtr> shapes_;
};

}

#endif /*SHAPE_GROUP_HPP_*/
