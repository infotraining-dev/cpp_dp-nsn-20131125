#include "bridge.hpp"

using namespace std;

int main()
{
	Abstraction* ab = new RefinedAbstraction();

	// Set implementation and call
	ConcreteImplementorA* implA = new ConcreteImplementorA();
	ab->set_implementor(implA);
	ab->operation();

	// Change implemention and call
	ConcreteImplementorB* implB = new ConcreteImplementorB();
	ab->set_implementor(implB);
	ab->operation();


	delete implA;
	delete implB;
	delete ab;
}
