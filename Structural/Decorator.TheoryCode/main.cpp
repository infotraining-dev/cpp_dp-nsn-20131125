#include "decorator.hpp"

int main()
{
	 // Create ConcreteComponent and two Decorators
	 ConcreteComponent* c = new ConcreteComponent();
	 ConcreteDecoratorA* d1 = new ConcreteDecoratorA(c);
	 Component* d2 = new ConcreteDecoratorB(d1);

	 d2->operation();

	 std::cout << std::endl;

	 delete d2;
}
