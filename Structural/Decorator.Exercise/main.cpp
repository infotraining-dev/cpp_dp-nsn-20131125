#include <memory>
#include "coffeehell.hpp"

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename T>
    CoffeeBuilder& create_base()
    {
        coffee_.reset(new T());
        return *this;
    }

    template <typename T>
    CoffeeBuilder& add()
    {
        Coffee* temp = coffee_.release();
        coffee_.reset(new T(temp));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
    Coffee* cf = new ExtraEspresso(new Whipped(new Whisky(new Whisky(new Whisky(new Espresso())))));

	std::cout << "Description: " << cf->get_description() << "; Price: " << cf->get_total_price() << std::endl;
	cf->prepare();

	delete cf;

    CoffeeBuilder cb;

    cb.create_base<Espresso>();
    cb.add<Whisky>().add<Whisky>().add<Whipped>().add<ExtraEspresso>();

    auto cf2 = cb.get_coffee();

    cf2->prepare();
}
